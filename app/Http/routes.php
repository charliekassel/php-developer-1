<?php

use App\Http\Controllers\VideoController;
use App\App;


// In my opinion version major should be hard tied to this codebase as supporting multiple versions
// within the same repository will ultimately lead to confusion and messy code.
// This allows for major updates to have a completely different architecture 
// without compromise or even be written in other languages.
$app->group(['prefix' => 'api/v'.App::VERSION], function() use ($app) {
    
    $app->post('/videos', [
        'as' => 'api.post',
        'uses' => VideoController::class . '@storeVideo'
    ]);

});
