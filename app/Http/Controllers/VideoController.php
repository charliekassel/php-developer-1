<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Storage;
use App\Libraries\FileInfo;


class VideoController extends Controller
{
    /**
     * @var FileInfo
     */
    private $fileInfo;

    /**
     * Constructor
     * @param FileInfo $fileInfo
     */
    public function __construct(FileInfo $fileInfo)
    {
        $this->fileInfo = $fileInfo;
    }


    /**
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function storeVideo(Request $request)
    {
        if (!$request->hasFile('videoFile')) {
            return response(
                ['error' => 'no file'],
                422,
                ['Content-Type' => 'application/json']
            );
        }

        $this->validate($request, [
            'videoFile' => 'required | mimes:mp4,mov,ogg,qt,mpg,mpeg | max:100000'
        ]);

        $file = Storage::store($request->file('videoFile'));

        return response()->json([
            'data' => $this->fileInfo->analyze($file)
        ]);        
    }
}
