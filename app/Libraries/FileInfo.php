<?php

namespace App\Libraries;

use getID3;
use App\Libraries\Sanitize;
use Symfony\Component\HttpFoundation\File\File;

class FileInfo
{
    /**
     * @var getID3
     */
    private $getID3;

    /**
     * @param getID3 $getID3
     */
    public function __construct(getID3 $getID3)
    {
        $this->getID3 = $getID3;
    }

    /**
     * @param Symfony\Component\HttpFoundation\File\File $file
     * @return Array
     */
    public function analyze(File $file)
    {
        $name = $file->getPathName();
        $info = $this->getID3->analyze($name);

        return Sanitize::convertToUTF8($info);
    }
}
