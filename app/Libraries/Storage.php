<?php

namespace App\Libraries;

use Illuminate\Http\UploadedFile;

class Storage {

    /**
     * @var string
     */
    const STORAGE_PATH = '/storage/uploads';

    /**
     * @param Illuminate\Http\UploadedFile $file
     * @return Symfony\Component\HttpFoundation\File\File
     */
    public static function store(UploadedFile $file)
    {
        return $file->move(base_path().self::STORAGE_PATH, $file->getClientOriginalName());
    }
}