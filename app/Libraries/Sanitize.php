<?php

namespace App\Libraries;

class Sanitize {
    /**
     * Converts data to utf8 encoding
     * @param array|string $mixed
     * @return $mixed
     */     
    public static function convertToUTF8($mixed) {
        if (is_array($mixed)) {
            foreach ($mixed as &$value) {
                $value = self::convertToUTF8($value);
            }
        } elseif (is_string($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }
}
