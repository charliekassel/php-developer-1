<?php

use App\App;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\VideoController;

class VideoPostTest extends TestCase {
    

    public function testNullFilePost()
    {
        $this->post('/api/v'.App::VERSION.'/videos', [
            'videoFile' => null
        ]);

        $this->assertEquals(422, $this->response->status());
        $this->shouldReturnJson();
        $this->seeJson(['error' => 'no file']);
    }

    public function testDummyFilePost()
    {
        $file = new UploadedFile(storage_path('uploads/').'dummy.mp4', 'dummy.mp4', 'video/quicktime', null, null, true);

        $response = $this->call(
            'POST',
            '/api/v'.App::VERSION.'/videos',
            [],
            [],
            ['videoFile' => $file]
        );

        $this->assertEquals(200, $this->response->status());
        $this->shouldReturnJson();
        $this->seeJson([
            'filename' => 'dummy.mp4',
            'dataformat' => 'quicktime'
        ]);
    }



}