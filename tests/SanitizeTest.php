<?php

use App\Libraries\Sanitize;

class SanitizeTest extends TestCase
{

    /**
     * @return void
     */
    public function testUTF8String()
    {
        $str = 'abcd';
        $cnv = Sanitize::convertToUTF8($str);
        $this->assertSame($str, $cnv);
    }

    /**
     * @return void
     */
    public function testNonUTF8String()
    {
        $str = 'abc∞#¡';
        $cnv = Sanitize::convertToUTF8($str);
        $this->assertSame('abcâ#Â¡', $cnv);
    }

    public function testReturnsArray()
    {
        $arr = [
            'data' => [
                'a' => 1,
                'b' => 'abd',
                'c' => 'some string'
            ]
        ];
        $cnv = Sanitize::convertToUTF8($arr);
        $this->assertTrue(is_array($cnv));
    }

    public function testConvertsNonUTF8InArray()
    {
        $arr = [
            'data' => [
                'a' => 1,
                'b' => 'abd',
                'c' => '∞'
            ]
        ];
        $cnv = Sanitize::convertToUTF8($arr);
        $this->assertSame($cnv['data']['c'], 'â');
    }
}