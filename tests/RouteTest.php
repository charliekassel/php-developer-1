<?php

use App\App;

class RouteTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetRootRoute()
    {
        $this->get('/');
        
        $this->assertEquals(404, $this->response->status());
    }

    /**
     * @return void
     */
    public function testGetRoute()
    {
        $this->get('/api/v'.App::VERSION.'/videos');

        $this->assertEquals(405, $this->response->status());
    }

    /**
     * @return void
     */
    public function testPostRoute()
    {
        $this->post('/api/v'.App::VERSION.'/videos');
        
        $this->assertEquals(422, $this->response->status());
        $this->seeJsonEquals(['error' => 'no file']);
    }
}
